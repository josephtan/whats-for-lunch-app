## Whats For Lunch App
An app which fetches json data of recipes and ingredients based on best-before and use-by dates once button is clicked.
List will include the ingredients for each recipe that has not past the use-by date and sorted to ascending order by best-before date. 

### Requrements
This app is built on create-react-app which requires node.js 6 or higher, npm and yarn on your machine.

### Installation
Clone this repository into a randomly named directory and from the command line type in "yarn install". No other need for other install commands as the package.json handles it all.

### Build and Running

Type "yarn build" to build and compress the project. All project files and assets will be outputted to the build folder.
Type "yarn start" to start the local server and run the project development environment.


### Testing
Create react app comes with jest for unit testing. To activate unit test type in the command "yarn test" within the project directory folder. Please choose the options provided on the command interface.

### Notes
1. Make sure the folder has correct CHMOD permissions before begining "yarn test" as jest will create a snapshot folder when testing.

2. There are multiple branches in this git repo and the "master" branch contains merged final from these git branches.

3. All source files are located in the "src" folder. Additional assets are within the "assets" folder.