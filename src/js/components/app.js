import React, { Component } from 'react';
import '../../css/app.scss';
import RecipeRequest from "./utils/recipeRequest";

class App extends Component {
  constructor (props){
        super(props);
        this.state = {active: this.props.active};
  }

  toggleShow(){
     if(this.state.active === false){
         this.setState({active:!this.props.active});
     }
     if(this.state.active === true){
         this.setState({active:this.props.active});
     }
  }
  render() {
      const isActive = this.state.active === true;
    return (
      <div className="app block-flex flex-column height-100vh">
        <header className="app-header">
            <h1><span className="fas fa-utensils"></span>Lunch Recipe<span className="fas fa-utensils"></span></h1>
        </header>
          <main className="block-flex flex-1 flex-column height-100mh flex-align-center">

          <div className="block-flex flex-column justify-content-center flex-1 recipe-container">

              <button className={this.state.active ? "btn margin-none bg-green-regular rounded-10px border-2-green text-white is-active flex-align-center ":
                  "btn margin-auto bg-green-regular rounded-10px border-2-green text-white flex-align-center"} target="_blank" rel="noopener noreferrer" onClick={this.toggleShow.bind(this)}>
                  <span className="fas fa-hamburger"></span> What is my Lunch
              </button>
              {isActive ? (<RecipeRequest/>) : (" ")}
          </div>
          </main>
      </div>
    );
  }
}

export default App;

App.defaultProps = {
    active: false
};