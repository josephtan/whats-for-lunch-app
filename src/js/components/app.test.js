import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import RecipeRequest from "./utils/recipeRequest";
import { shallow } from 'enzyme';
it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

describe("recipeRequest", () => {
  it("renders correctly without crashing", () => {
    const component = shallow(<RecipeRequest />);
    expect(component).toMatchSnapshot();
  });
});


describe("recipeRequest", () => {
  it("should change state active to true when button clicked", () => {
    const wrapper = shallow(<App />);
    wrapper.instance( 'toggleShow');
    wrapper.setState({ active: false }, () => {
      expect(wrapper.state()).toEqual({ active: false}); // passed
    });

    wrapper.find(".btn").simulate('click');
    wrapper.update();
    wrapper.setState({active:true},()=>{
      expect(wrapper.state()).toEqual({ active: true}); // passed
    });
  });
});