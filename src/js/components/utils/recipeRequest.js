import React, { Component } from 'react';
import axios from "axios";

class RecipeRequest extends Component {
    constructor (props){
        super(props);
        this.recipejsonurl = "http://www.mocky.io/v2/5c85f7a1340000e50f89bd6c";
        this.ingredientjsonurl = "https://www.mocky.io/v2/5c9075843600004b00f100fa";
        this.state = {active: this.props.active, recipejson:[], ingredientsjson:[]}
    }
     componentDidMount() {
         axios.all([
             axios.get(this.recipejsonurl),
             axios.get(this.ingredientjsonurl)
         ]).then(axios.spread((resRecipe, resIngredients) => {
            this.setState({recipejson: resRecipe.data.recipes, ingredientsjson: resIngredients.data.ingredients});
             })).catch((err)=>{
             console.log(err);
             /** Catch and output error message if fail to get url **/
         });
    }

    fetchSortRecipes(){
        const date = new Date(); /** to change current date add string i.e "2019-03-22" for testing purposes, leave empty for today's date **/
        const currentDate = date.toISOString().substring(0, 10);
        let recipeArray = [];
        let ingredientArray = [];
        let mergeArrays = [];
        let currentRecipeList =[];
        let finalRecipeList = [];
        Object.keys(this.state.recipejson).forEach((index) => {
            recipeArray.push(this.state.recipejson[index]);
        });
        Object.keys(this.state.ingredientsjson).forEach((index) => {
            ingredientArray.push(this.state.ingredientsjson[index]);
        });

        let dateParseCurrent = Date.parse(currentDate);

        /** Create and merge 2 arrays with similar values with the .includes method and ignores ingredients from recipe JSON if not matched with the ingredients JSON **/
        recipeArray.map((object) => {
            ingredientArray.map((items) => {
                if(object.ingredients.includes(items["title"])){
                    /** assign matching ingredients and titles from 2nd object to a new array **/
                    mergeArrays.push({"title":object.title,"ingredients":items.title,"best-before":items["best-before"],"use-by":items["use-by"]});
                }

            });
        });

         /** sort ingredients is past its ​best-before​ date AND is still within its ​use-by​ date and push ingredients  not  past its ​use-by​ date **/
        mergeArrays.map((object) =>{
            let dateParseUsedBy = Date.parse(object["use-by"]);
            let dateParseBest = Date.parse(object["best-before"]);
            if((dateParseBest > dateParseCurrent) !==false && (dateParseUsedBy >= dateParseCurrent) !== false){
                currentRecipeList.push(object);
            }
        });

        /**  sorted to bottom of list **/
         currentRecipeList.sort((a, b) => {
            let dateParseUsedBy = Date.parse(a["use-by"]);
            let dateBestB = Date.parse(b["best-before"]);
            return ( dateParseUsedBy - dateBestB);
        });

        /**  Merge repeated keys with different values and push to finalRecipeList as final result array **/
        currentRecipeList.forEach((item) => {
            let existing = finalRecipeList.filter((object) => {
                return object.title === item.title;
            });
            if (existing.length) {
                let existingIndex = finalRecipeList.indexOf(existing[0]);
                finalRecipeList[existingIndex].ingredients = finalRecipeList[existingIndex].ingredients.concat(item.ingredients);
            } else {
                if (typeof item.ingredients == 'string')
                    item.ingredients = [item.ingredients];
                finalRecipeList.push(item);
            }
        });


       return(
            <ul className={this.state.active? "recipes-list":"recipes-list is-active"}>{finalRecipeList.map((item,index) =>
                <li key={index} className="text-left">
                    <h2 className="border-top-2-green text-center">{item.title}</h2>
                    <h3>What you'll need:</h3>
                    <div> {finalRecipeList[index]["ingredients"].join(", ")}</div>
                </li>)}
            </ul>);

    }

    render(){
         return(<div>{this.fetchSortRecipes()}</div>);
    }
}

export default RecipeRequest;

RecipeRequest.defaultProps = {
    active: false
};