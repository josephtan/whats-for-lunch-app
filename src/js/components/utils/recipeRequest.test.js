import React, { Component } from 'react';
import {shallow} from "enzyme/build";
import RecipeRequest from "../utils/recipeRequest";
import MockAdapter from 'axios-mock-adapter';
import axios from "axios";

const flushPromises = () => new Promise(resolve => setImmediate(resolve));

it("mocky.io promises request", async () => {
    const wrapper = shallow(<RecipeRequest />);
    await flushPromises();
    wrapper.update();
});


describe("recipeRequest",()=> {
    it("should render list map correctly", () => {
        const wrapper = shallow(<RecipeRequest/>);
        wrapper.instance( 'fetchSortRecipes');
        wrapper.update();
        expect(wrapper.exists("ul.recipes-list")).toEqual(true);
        console.log(wrapper.debug("ul.recipes-list li"));
    });
});